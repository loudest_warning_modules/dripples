## Dripples build notes
**Please read it entirely first, it could be useful.**

## Building advices
Since this is mostly SMD, I'd recommend soldering all SMD parts first (except SMD trimmers). Then, wash your board depending on the flux used, and solder the board interconnect headers, then the trimmers, and finally the switches. If you are using water-soluble flux, make sure **you wash your board**. Since the filters are based on currents (current in, current out) this slightly conductive type of flux will not help.

Once it is done, you can solder all pots and power input.

It is not the most complex build out there, but it certainly is long a tedious. Take your time to avoid mixing up values.

There are several test points on the boards that could help you to check everything is fine; they are labeled with the voltage you should expect from them (either +10V or -10V). You can check if each filter is oscillating on the individual outputs on the back (EXP_OUTx).

#### Additional hardware
You may need 6 M3 screws with 10mm plastic spacers. M3 washers may be handy too, I use plastic (nylon) ones to adjust the height depending on the connectors used between the boards.

## Circuitry notes

The top board mostly contains CV circuits, while the bottom board has all the "core" circuitry (filters, morph and panning circuits).

#### Filters
The Ripples V2 circuitry is mostly unchanged. I got rid of the LEDs and the ending LP VCA (since we are using the LM13700 other half for filter B feedback path). The filter cores remain the same.

The switches for the poles-selection are a bit expensive. If you're sure about choosing either one or the other configuration (-12dB or -24dB), you can solder the pins together directly.

One important thing: make sure to order **side adjust** 25-turn trimmers for the 1V/oct trim. Also, they'll need to be soldered on the bottom side of the top PCB (or else you won't be able to fit the panel in!).

#### Scan/morph
The footprint for the SSI2162 can be a bit tedious to solder, since it is not a footprint suitable for hand-soldering. But with care, it can be done.

The CV circuit may be adjusted to taste. The resistor across +12V and the emitters may be modified a bit, as it changes the "plateau" size of the control voltages.
The resistor tied across -12V and the bases modifies the size of the center zone, i.e when the knob is at noon.

#### Expanders
All outputs for each filter are available on the back, if one wants to make its own expander with all filters outputs available simultaneously. They need to be buffered though, as their level is way too low to be usable "as-is" in a modular (hint: simple op-amp buffers with suitable gain should suffice, the values can be taken from the original Ripples circuitry).

Also, there are morph outputs available, if one want to bypass the panning section.

#### Panning
The panning circuitry can be calibrated to taste. [calibration procedure here].

If you find the output level to be a bit weak, you can adjust (increase) the resistor in the final op-amp feedback (with the given values, it should be around the input level).

## Components
#### Capacitors
Ceramics C0G caps may be used almost everywhere, as for decoupling, I like X7R 100nF.

All footprints are 0805, except for the AC-coupling caps at the end of the pole-mixing section (those are 1206).

#### Resistors
Standard metal-film, 0805, 1% precision are fine. There are a few places where there must be 0.5% or better resistors (specified in the BOM), especially in the filter core and the poles mixing.

#### ICs
One day OPA16xx may be available again, but at the moment they are unobtainable. Standard TL07x will work well, however, in the filter core, offset matters. There are a few TL07x versions that have better offset specs, better use these there, if possible.
