## Dripples
**_A Loudest Warning adaptation of Mutable Instruments Ripples filter, in a dual package with some twists._**

As it is described by Mutable Instruments:
>>>
"Ripples is a classic, big-sounding analog four-pole filter.
Self-oscillation is available for all modes and produces a clean sine-wave on the low-pass output. Unlike many classic 4-pole designs, this filter does not suffer from “loudness drop” when resonance is increased. The resonance loudness compensation circuit brings a slight tone coloration reminiscent of Japanese classics from the 80s, with a very round and liquid resonance."
>>>

![alt text](dripples.JPG "built module")

In this version, we used the core of Ripples, but with some modifications:
* The low-pass VCA is no more;
* No more LEDs;
* Individual outputs are not available anymore...
* ... But instead there a voltage-controlled scan/morph between them.
* Last but not least, a panning system allows to mix or mirror the audio between the two filters, thus creating interesting stereo effects.

Thanks to bananas, it is quite easy to change the routing on the fly.

### Features
* Two SSI2164 filters, based on Mutables Instruments Ripples filter.
* For each filter:
   * Cutoff and resonance control.
   * 1V/oct input, not temperature compensated.
   * CV control over resonance.
   * Clean input and overdrive input, with distortion amount.
   * Modulation input (linear) with attenuverter.
   * A scan circuit, that allows to morph between LP, BP and HP.
   * CV control over scan circuit.
   * User-selectable poles on the back (either 12dB or 24dB).
* One panning circuit, that morph/crossfades between each filter output, allowing each filter to be heard alone at one side, or blended in between.

The morph circuitry was not really designed to properly "blend" between the responses, but rather to be able to scan rapidly between LP, BP and HP with CV.


### Repository contents
- The hardware files:
    - The whole KiCad project
    - The Bill of Materials (BoM)
    - The gerber files used for the batch
    - A pdf export of the schematics
- Panel designs:
    - The SVG for the panel design
    - The exported pdf
- Some documentation:
    - Build notes on circuits, components selection
    - Original schematics
    - Previous circuit versions

See the **build_doc** folder for build notes and circuit tweaks.

Currently in development.

For more information see https://mutable-instruments.net/modules/ripples/


## Technical specs

4U module for the [Loudest Warning](https://loudestwarning.tumblr.com/4Umodular) format.

Outputs:

Inputs:

Depth:

Current draw: **?mA** on -12V, **?mA** on +12V.

Many thanks to Mutable Instruments for the wonderful designs, and all helpful wigglers out there (cygmu, guest, and others!).
